package at.fh.oo.Jukebox;

import java.util.Scanner;

public class Box {

    private Scanner myScanner = new Scanner(System.in);
    private Magazin myMagazin = new Magazin();
    private Boolean loaded;
    private String Platte;
    private int Songnumber;

    public void load(){
        System.out.println("Geben Sie den Namen der Platte ein, die in den Spieler eingelegt werden soll");
        Platte = myScanner.nextLine();

        myMagazin.load(Platte);
        loaded = true;
        Platte = "";
    }

    public void play(){
        if(loaded){
            System.out.println("Welche Platte " + Platte + " wollen Sie spielen lassen?");
            Platte = myScanner.nextLine();
            myMagazin.play(Platte);
        }
        else{
            System.out.println("Es muss zuerst eine Platte in den Spieler eingelegt werden");
        }
    }

    public void remove(){
        myMagazin.removeRecord(Platte);
    }

    public void addRecord(){
        myMagazin.addRecord();
    }

    public void search(){
        System.out.println("Welche Platte wollen Sie suchen?");
        String SearchedRecord = myScanner.nextLine();
        myMagazin.search(SearchedRecord);
    }

    public void getSumOfTime(){
        myMagazin.getSumOfTime();
    }
}
