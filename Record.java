package at.fh.oo.Jukebox;

import java.util.ArrayList;
import java.util.List;

public class Record {

    private String Recordname;
    public List<Title> Titlelist = new ArrayList();

    public Record(String Recordname){
        this.Recordname = Recordname;
        for(int i = 0; i < 5; i++){
            Titlelist.add(new Title("Didi" + i, 100 + i));
        }
    }

    public String getRecordname() {
        return Recordname;
    }

    public void getSum(){

        int iTime = 0;

        for(int i = 0; i < Titlelist.size(); i++) {
            iTime = iTime + Titlelist.get(i).getTime();
        }

        System.out.println("Die Gesamtwiedergabezeit beträgt: " + iTime);
    }
}
