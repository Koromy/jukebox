package at.fh.oo.Jukebox;

public class Title {

    private String title;
    private int time;

    public Title(String title,int time){
        this.title = title;
        this.time = time;
    }

    public String getTitle(){
        return this.title;
    }

    public int getTime() {
        return this.time;
    }
}
