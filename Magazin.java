package at.fh.oo.Jukebox;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Magazin {

     List<Record> List = new ArrayList();

    public void addRecord()
    {
        Record Test = new Record("Test");
        List.add(Test);

        System.out.println("Geben Sie den Namen der Platte an, die Sie hinzufuegen möchten");
        Scanner myScanner = new Scanner(System.in);
        String Recordname = myScanner.nextLine();

        Record Platte = new Record(Recordname);

        if(List.size() <= 50) {
            List.add(Platte);
            System.out.println("Ihre Platte wurde eingelegt");
            System.out.println(List.get(0).getRecordname());
        }
        else if(List.size() > 50){
            System.out.println("Es sind schon 50 Platten eingelegt, entfernen Sie zuerst eine alte, bevor Sie eine neue einlegen");
        }
        else
        {
            System.out.println("FEHLER");
        }
    }

    public void removeRecord(String Platte){
        List.remove(Platte);
        System.out.println("Die Platte wurde entfernt");
    }

    public void getSumOfTime(){
        Record Record = new Record("Test3");

        Record.getSum();
    }

    public void search(String Platte){
        int id = List.indexOf(Platte);
        if(id == List.indexOf(Platte)){
            System.out.println("Die Platte " + Platte + " gibt es");
        }
        else {
            System.out.println("Die Platte " + Platte + " ist nicht im Spieler");
        }
    }

    public void load(String Platte){

        Boolean isthere= false;

        for(int i = 0; i < List.size(); i++){
            if(Platte == List.get(i).getRecordname()){
                isthere = true;
            }
        }

        if(isthere = true){
            System.out.println("Die Platte ist nun im Spieler");
        }
        else{
            System.out.println("FEHLER");
        }
    }

    public void play(String Platte){
        Record Record = new Record(Platte);

        System.out.println("Welchen Song wollen Sie spielen:\n");
        for(int i = 0; i < Record.Titlelist.size(); i++){
            System.out.println("(" + i + ") " + Record.Titlelist.get(i).getTitle());
        }
        Scanner myScanner = new Scanner(System.in);
        int i = myScanner.nextInt();

        System.out.println("Der Song " + Record.Titlelist.get(i).getTitle() + " wird nun gespielt");
    }
}
