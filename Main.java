package at.fh.oo.Jukebox;

import java.util.Scanner;

public class Main {

    public static void main(String[] args){
        Box myBox = new Box();
        Scanner myScanner = new Scanner(System.in);

        while(true) {
            System.out.println("Auswahl:\n1 play\n2 load\n3 remove\n4 Platte hinzufuegen\n5 Gesamtspielzeit\n6 Platte suchen");
            int Auswahl = myScanner.nextInt();

            if(Auswahl == 1) {
                myBox.play();
            }
            else if(Auswahl == 2){
                myBox.load();
            }
            else if(Auswahl == 3){
                myBox.remove();
            }
            else if(Auswahl == 4){
                myBox.addRecord();
            }
            else if(Auswahl == 5){
                myBox.getSumOfTime();
            }
            else if(Auswahl == 6){
                myBox.search();
            }
        }
    }
}
